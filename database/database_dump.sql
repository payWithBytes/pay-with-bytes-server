--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: User_Files_Metadata; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "User_Files_Metadata" (
    user_id character varying(50) NOT NULL,
    file_id_hash character varying(128) NOT NULL,
    file_metadata_wrapper character varying(16384) NOT NULL
);


ALTER TABLE public."User_Files_Metadata" OWNER TO postgres;

--
-- Name: User_Files_Metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "User_Files_Metadata"
    ADD CONSTRAINT "User_Files_Metadata_pkey" PRIMARY KEY (user_id, file_id_hash);


--
-- PostgreSQL database dump complete
--
