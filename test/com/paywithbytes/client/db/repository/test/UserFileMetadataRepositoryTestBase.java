package com.paywithbytes.client.db.repository.test;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.List;
import java.util.UUID;

import javax.xml.bind.DatatypeConverter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.paywithbytes.server.db.UserFileMetadataRepository;
import com.paywithbytes.server.db.exceptions.RepositoryException;
import com.paywithbytes.server.serialization.JsonHelper;
import com.paywithbytes.communication.beans.UserFileMetadataWrapper;
import com.paywithbytes.jfec.wrapper.EncodingMetadata;
import com.paywithbytes.persistency.beans.Challenge;
import com.paywithbytes.persistency.beans.Types;
import com.paywithbytes.persistency.beans.UserChunkMetadata;
import com.paywithbytes.persistency.beans.UserFileMetadata;

/**
 * Test the Repository interface. This is just the Base, not the actual test.
 * 
 * @param <T>
 */
public abstract class UserFileMetadataRepositoryTestBase<T extends UserFileMetadataRepository> {

	private T repository;

	protected abstract T createRepositoryInstance();

	@Before
	public void setUp() {
		repository = createRepositoryInstance();
	}
	
	private String toHexString(byte[] array) {
	    return DatatypeConverter.printHexBinary(array);
	}

	
	@Test
	public void testInsertAndSelect() throws NoSuchAlgorithmException, NoSuchProviderException, RepositoryException {
		
		String userId = "Lionel_Messi";
		// Create object to insert
		Challenge challenge = new Challenge("1912", "+", "30");
		UserFileMetadata fileMetadata = new UserFileMetadata(UUID.randomUUID().toString(), "ThankYou.txt",
				null, null, challenge, 1942.0, Types.Public, 123123, new EncodingMetadata(3, 2, 123123));

		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-1", 1912, "192.168.2.1"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-2", 1912, "192.168.2.2"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-3", 1912, "192.168.2.3"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-4", 1912, "192.168.2.4"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-5", 1912, "192.168.2.5"));
		
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] theDigest = md.digest(fileMetadata.getUUID().getBytes());
		
		// Insert
		repository.insert(userId, this.toHexString(theDigest), JsonHelper.toJson(fileMetadata, UserFileMetadata.class).getBytes());

		// Retrieve
		byte[] retrievedItemBytes = repository.select(userId, this.toHexString(theDigest));

		UserFileMetadata retrievedItem = JsonHelper.fromJson(new String(retrievedItemBytes), UserFileMetadata.class);
		
		// Compare
		Assert.assertTrue(fileMetadata.equals(retrievedItem));
	}

	@Test
	public void testInsertAndDeleteAndSelect() throws NoSuchAlgorithmException, NoSuchProviderException,
			RepositoryException {
		String userId = "Cristiano_Ronaldo";

		// Create object to insert
		Challenge challenge = new Challenge("1912", "+", "30");
		UserFileMetadata fileMetadata = new UserFileMetadata(UUID.randomUUID().toString(), "ThankYou.txt",
				null, null, challenge, 1942.0, Types.Public, 123123, new EncodingMetadata(3, 2, 123123));

		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-1", 1912, "192.168.2.1"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-2", 1912, "192.168.2.2"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-3", 1912, "192.168.2.3"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-4", 1912, "192.168.2.4"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-5", 1912, "192.168.2.5"));

		// Insert
		repository.insert(userId, fileMetadata.getUUID(), JsonHelper.toJson(fileMetadata, UserFileMetadata.class).getBytes());

		// Retrieve
		byte[] retrievedItemBytes = repository.select(userId, fileMetadata.getUUID());
		UserFileMetadata retrievedItem = JsonHelper.fromJson(new String(retrievedItemBytes), UserFileMetadata.class);
				
		// Compare
		Assert.assertTrue(fileMetadata.equals(retrievedItem));

		// Delete
		repository.delete(userId, fileMetadata.getUUID());

		// Trying to select again
		retrievedItemBytes = repository.select(userId, fileMetadata.getUUID());

		// The retrieved item should be NULL at this point
		Assert.assertNull(retrievedItemBytes);
	}

	@Test
	public void testInsertAndUpdate() throws NoSuchAlgorithmException, NoSuchProviderException, RepositoryException {
		String userId = "Alanis_Morissete";
		
		// Create object to insert
		Challenge challenge = new Challenge("1912", "+", "30");
		UserFileMetadata fileMetadata = new UserFileMetadata(UUID.randomUUID().toString(), "ThankYou.txt",
				null, null, challenge, 1942.0, Types.Public, 123123, new EncodingMetadata(3, 2, 123123));

		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-1", 1912, "192.168.2.1"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-2", 1912, "192.168.2.2"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-3", 1912, "192.168.2.3"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-4", 1912, "192.168.2.4"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-5", 1912, "192.168.2.5"));

		// Insert
		repository.insert(userId, fileMetadata.getUUID(), JsonHelper.toJson(fileMetadata, UserFileMetadata.class).getBytes());

		// Modify the object
		Challenge modifiedChallenge = new Challenge("20", "+", "50");
		fileMetadata.setChallengeAssociated(modifiedChallenge);
		
		repository.update(userId, fileMetadata.getUUID(), JsonHelper.toJson(fileMetadata, UserFileMetadata.class).getBytes());

		// Retrieve
		byte[] retrievedItemBytes = repository.select(userId, fileMetadata.getUUID());
		UserFileMetadata retrievedItem = JsonHelper.fromJson(new String(retrievedItemBytes), UserFileMetadata.class);
		
		Assert.assertTrue(modifiedChallenge.equals(retrievedItem.getChallengeAssociated()));
	}
	
	@Test
	public void testMultipleInsertAndSelect() throws NoSuchAlgorithmException, NoSuchProviderException, RepositoryException {
		
		String userId = UUID.randomUUID().toString();
		// Create object to insert
		Challenge challenge = new Challenge("1912", "+", "30");
		UserFileMetadata fileMetadataOne = new UserFileMetadata(UUID.randomUUID().toString(), "ThankYou.txt",
				null, null, challenge, 1942.0, Types.Public, 123123, new EncodingMetadata(3, 2, 123123));

		fileMetadataOne.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-1", 1912, "192.168.2.1"));
		fileMetadataOne.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-2", 1912, "192.168.2.2"));
		fileMetadataOne.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-3", 1912, "192.168.2.3"));
		fileMetadataOne.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-4", 1912, "192.168.2.4"));
		fileMetadataOne.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-5", 1912, "192.168.2.5"));

		
		// Create object to insert
		Challenge challengeTwo = new Challenge("1912", "+", "31");
		UserFileMetadata fileMetadataTwo = new UserFileMetadata(UUID.randomUUID().toString(), "ThankYou.txt",
				null, null, challengeTwo, 1942.0, Types.Public, 123123, new EncodingMetadata(3, 2, 123123));

		fileMetadataTwo.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-1", 1912, "192.168.2.1"));
		fileMetadataTwo.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-2", 1912, "192.168.2.2"));
		fileMetadataTwo.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-3", 1912, "192.168.2.3"));
		fileMetadataTwo.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-4", 1912, "192.168.2.4"));
		fileMetadataTwo.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-5", 1912, "192.168.2.5"));
		
		UserFileMetadataWrapper itemOneToInsert = new UserFileMetadataWrapper(JsonHelper.toJson(fileMetadataOne, UserFileMetadata.class).getBytes(), null, null, "ccp1912");
		UserFileMetadataWrapper itemTwoToInsert = new UserFileMetadataWrapper(JsonHelper.toJson(fileMetadataTwo, UserFileMetadata.class).getBytes(), null, null, "ccp1912");
		
		// Insert
		repository.insert(userId, fileMetadataOne.getUUID(), JsonHelper.toJson(itemOneToInsert, UserFileMetadataWrapper.class).getBytes());
		// Insert
		repository.insert(userId, fileMetadataTwo.getUUID(), JsonHelper.toJson(itemTwoToInsert, UserFileMetadataWrapper.class).getBytes());

		// Retrieve
		List<UserFileMetadataWrapper> retrievedItems = repository.select(userId);

		// Compare TODO: I'm sure this is not 100% deterministic, fix this.
		UserFileMetadata itemOneRetrieved = JsonHelper.fromJson(new String(retrievedItems.get(1).getUserFileMetadata()), UserFileMetadata.class);
		UserFileMetadata itemTwoRetrieved = JsonHelper.fromJson(new String(retrievedItems.get(0).getUserFileMetadata()), UserFileMetadata.class);
		
		Assert.assertTrue(fileMetadataOne.equals(itemOneRetrieved));
		Assert.assertTrue(fileMetadataTwo.equals(itemTwoRetrieved));
		
	}
	

	
	

}
