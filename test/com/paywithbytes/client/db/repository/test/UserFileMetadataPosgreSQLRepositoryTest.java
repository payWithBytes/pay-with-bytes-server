package com.paywithbytes.client.db.repository.test;

import com.paywithbytes.server.db.UserFileMetadataPostgreSQLRepository;
import com.paywithbytes.server.db.UserFileMetadataRepository;


/**
 * This initializes the actual SQLite Repository for the base test.
 * 
 */
public class UserFileMetadataPosgreSQLRepositoryTest extends
		UserFileMetadataRepositoryTestBase<UserFileMetadataRepository> {

	@Override
	protected UserFileMetadataRepository createRepositoryInstance() {
		return new UserFileMetadataPostgreSQLRepository();
	}

}
