package com.paywithbytes.server.controllers;



import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.paywithbytes.communication.beans.RequestResponse;
import com.paywithbytes.communication.beans.UserFileMetadataWrapper;
import com.paywithbytes.server.db.UserFileMetadataPostgreSQLRepository;
import com.paywithbytes.server.db.UserFileMetadataRepository;
import com.paywithbytes.server.db.exceptions.RepositoryException;
import com.paywithbytes.server.serialization.JsonHelper;

@Controller
public class RecoveryMetadataController  {

	protected final Log log = LogFactory.getLog(getClass());

	@RequestMapping(value = "Recovery/{userId}", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String info(@PathVariable String userId) {
		
		final UserFileMetadataRepository repository = new UserFileMetadataPostgreSQLRepository();
		RequestResponse response = new RequestResponse(HttpServletResponse.SC_NOT_FOUND, "Items not found", null);
		
		try {
			
			List<UserFileMetadataWrapper> userFileMetadataWrapperList = repository.select(userId);
			
			if(userFileMetadataWrapperList != null && userFileMetadataWrapperList.size() != 0){
				response.setValue(JsonHelper.toJson(userFileMetadataWrapperList, List.class));
				response.setStatus(HttpServletResponse.SC_OK);
				response.setDescription("Data retrieved successfully.");			
			}
			
		} catch (RepositoryException e) {
			log.error("Error when trying to retrieve data for userId [" + userId + "]", e);
		}
		
		return JsonHelper.toJson(response, RequestResponse.class);
		
    }

}
