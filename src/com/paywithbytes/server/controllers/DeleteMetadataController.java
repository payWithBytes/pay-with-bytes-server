package com.paywithbytes.server.controllers;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.paywithbytes.communication.beans.RequestResponse;
import com.paywithbytes.communication.beans.UserFileMetadataWrapper;
import com.paywithbytes.server.db.UserFileMetadataPostgreSQLRepository;
import com.paywithbytes.server.db.UserFileMetadataRepository;
import com.paywithbytes.server.db.exceptions.RepositoryException;
import com.paywithbytes.server.serialization.JsonHelper;

@Controller
public class DeleteMetadataController  {

	protected final Log log = LogFactory.getLog(getClass());

	@RequestMapping(value = "Delete/{userId}", method = RequestMethod.DELETE, produces="application/json")
	public ResponseEntity<String> info(@PathVariable String userId, @RequestBody String userFileMetadataWrapperJSON) {
		
		UserFileMetadataWrapper userFileMetadataWrapper = JsonHelper.fromJson(userFileMetadataWrapperJSON, UserFileMetadataWrapper.class);
		
		final UserFileMetadataRepository repository = new UserFileMetadataPostgreSQLRepository();
		RequestResponse response = new RequestResponse(HttpServletResponse.SC_CONFLICT, "Items were not deleted", null);
		
		try {
			repository.delete(userId, userFileMetadataWrapper.getFileIdDigest());
			
			response.setStatus(HttpServletResponse.SC_OK);
			response.setDescription("Value deleted successfully.");
		} catch (RepositoryException e) {
			log.error("Error when trying to delete data for userId [" + userId + "] and fileIdDigest [" + userFileMetadataWrapper.getFileIdDigest() + "]", e);
		}
		
		return new ResponseEntity<>(JsonHelper.toJson(response, RequestResponse.class), HttpStatus.OK);
		
    }

}
