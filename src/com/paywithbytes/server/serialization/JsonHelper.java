package com.paywithbytes.server.serialization;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * We encapsulated the Json methods in this class. This will help in the future if we want to implement
 * another library, maybe a faster one (?).
 * 
 */
public class JsonHelper {

	private static final Gson gson = new GsonBuilder().create();

	/**
	 * Converts object to JSON
	 * 
	 * @param <T>
	 * 
	 * @return JSON string.
	 */
	public static <T> String toJson(Object o, Class<T> cls) {
		// I know it's bad to create a Gson object every time we called toJson() method, but
		// right now I don't want to messed up with the UserFileMetadata beans.
		return gson.toJson(o, cls);
	}

	/**
	 * 
	 * Convert json string value to type T
	 * 
	 * Example:
	 * 
	 * Retrieving a List of UserChunkMetadata:
	 * 
	 * JsonHelper.fromJsonArray(jsonVal, new TypeToken<List<UserChunkMetadata>>() {});
	 * 
	 * @param <T>
	 * 
	 * @param <T>
	 * @param jsonValue
	 * @return
	 */
	public static <T> T fromJsonArray(String jsonValue, TypeToken<T> type) {

		return gson.fromJson(jsonValue, type.getType());
	}

	/**
	 * Converts object to JSON
	 * 
	 * @param <T>
	 * 
	 * @return JSON string.
	 */
	public static <T> T fromJson(String jsonValue, Class<T> cls) {
		// I know it's bad to create a Gson object every time we called toJson() method, but
		// right now I don't want to messed up with the UserFileMetadata beans.
		return gson.fromJson(jsonValue, cls);
	}

	public static void main(String args[]) throws NoSuchAlgorithmException, NoSuchProviderException {
		/*
		 * UserFileMetadata fileMetadata = new UserFileMetadata(UUID.randomUUID().toString(), "ThankYouDude.txt",
		 * SymmetricEncryptionHelper.getRandomKey(),
		 * "30 + 1912", "Public",
		 * 400, 123123);
		 * 
		 * fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("jah-", "jah-1", 1912, "192.168.2.1"));
		 * fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("jah-", "jah-2", 1912, "192.168.2.2"));
		 * fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("jah-", "jah-3", 1912, "192.168.2.3"));
		 * fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("jah-", "jah-4", 1912, "192.168.2.4"));
		 * fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("jah-", "jah-5", 1912, "192.168.2.5"));
		 * 
		 * String jsonVal = JsonHelper.toJson(fileMetadata.getUserChunkMetadataList(), List.class);
		 * 
		 * JsonHelper.fromJsonArray(jsonVal, new TypeToken<List<UserChunkMetadata>>() {
		 * });
		 * 
		 * System.out.println(jsonVal);
		 */

	}
}
