package com.paywithbytes.server.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This class contains the connection to the database that it is only initialized once every time
 * we run the application.
 * 
 * TODO: Create a POOL of connections, this will be a huge bottleneck if used in this way.
 * 
 */
public class ConnectionHelper {

	protected static final Log log = LogFactory.getLog(ConnectionHelper.class);
	private static final String classForNameString = "org.postgresql.Driver";
	private static final String postgreSQLConnectionString = "jdbc:postgresql://localhost:5432/postgres";
	private static Connection connection;

	static {

		try {
			Class.forName(classForNameString);
			setConnection(DriverManager.getConnection(ConnectionHelper.postgreSQLConnectionString, "postgres", "ciclon"));
		} catch (SQLException | ClassNotFoundException e) {
			log.error(e.getMessage(), e);
		}

		log.info("Opened database successfully");
	}

	protected static Connection getConnection() {
		return connection;
	}

	private static void setConnection(Connection connection) {
		ConnectionHelper.connection = connection;
	}

	protected void closeConnection() throws SQLException {
		ConnectionHelper.getConnection().close();
	}

}
