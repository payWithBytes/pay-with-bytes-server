package com.paywithbytes.server.db;

import java.util.List;

import com.paywithbytes.communication.beans.UserFileMetadataWrapper;
import com.paywithbytes.server.db.exceptions.RepositoryException;

/**
 * We design the Repository with this interface because we might need to change
 * the PostgreSQL implementation some time in the future. If we do that, this interface
 * will make the change easier. Even the junit tests are written for this interface
 * and not for the specific implementation.
 */
public interface UserFileMetadataRepository {

	/**
	 * Insert the userFileMetadata in the database (or storage media) for the given userID.
	 * 
	 * @param userId
	 * @param userFileMetadata
	 * @throws RepositoryException
	 */
	public void insert(String userId, String fileIdHash, byte[] userFileMetadataWrapper) throws RepositoryException;

	/**
	 * Update the userFileMetadata in the database (or storage media) for the given userID.
	 * @param userId
	 * @param userFileMetadata
	 * @throws RepositoryException
	 */
	public void update(String userId, String fileIdHash, byte[] userFileMetadataWrapper) throws RepositoryException;

	/**
	 * Select the userFileMetadata from the database (or storage media) for the given userId.
	 * @param userId
	 * @param fileId
	 * @return
	 * @throws RepositoryException
	 */
	public byte[] select(String userId, String fileIdHash) throws RepositoryException;

	/**
	 * Select the Collection of userFileMetadatas' from the database (or storage media) for the given userId.
	 * @param userId
	 * @param fileId
	 * @return
	 * @throws RepositoryException
	 */
	public List<UserFileMetadataWrapper> select(String userId) throws RepositoryException;
	
	
	/**
	 * Delete the userFileMetadata from the database (or storage media)
	 * 
	 * @param uuid
	 */
	public void delete(String userId, String fileIdHash) throws RepositoryException;

}