package com.paywithbytes.server.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.paywithbytes.communication.beans.UserFileMetadataWrapper;
import com.paywithbytes.server.db.exceptions.RepositoryException;
import com.paywithbytes.server.serialization.JsonHelper;

/**
 * This is the implementation in SQLite for the UserFileMetadataRepository interface.
 * 
 */
public class UserFileMetadataPostgreSQLRepository extends ConnectionHelper implements UserFileMetadataRepository {

	private static final String tableName = "\"public\".\"User_Files_Metadata\"";
	
	private static final String tableFirstPKName = "user_id";
	private static final String tableSecondPKName = "file_id_hash";
	
	
	@Override
	public void insert(String userId, String fileIdHash, byte[] userFileMetadataWrapper) throws RepositoryException {

		PreparedStatement prep = null;
		try {
			String preSqlStmt = String.format("insert into %s values(?,?,?);", tableName);
			prep = getConnection().prepareStatement(preSqlStmt);
			/*
			 * 1. user_id character varying(50) NOT NULL
			 * 2. file_id character varying(50) NOT NULL
			 * 3. file_metadata character varying(2048) NOT NULL
			 */
			prep.setString(1, userId);
			prep.setString(2, fileIdHash);
			prep.setString(3, new String(userFileMetadataWrapper));

			prep.execute();
			log.info(String.format("Records inserted successfully : userId [%s], metadata UUID [%s]", userId, fileIdHash));

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new RepositoryException(String.format("Error when inserting data UUID [%s] for userId [%s]", fileIdHash, userId),
					e);
		} finally {
			if (prep != null)
				try {
					prep.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}

	}

	@Override
	public void delete(String userId, String fileIdHash) throws RepositoryException {
		String preSqlStmt = String.format("delete from %s where %s = ? and %s = ?", tableName, tableFirstPKName, tableSecondPKName);

		PreparedStatement prep = null;

		try {
			prep = getConnection().prepareStatement(preSqlStmt);
			prep.setString(1, userId);
			prep.setString(2, fileIdHash);
			prep.executeUpdate();
			log.info(String.format("Record deleted successfully : metadata UUID [%s] for user [%s]", fileIdHash, userId));
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
			throw new RepositoryException(String.format("Error when inserting data UUID [%s] for user [%s]", fileIdHash, userId), e);
		} finally {
			if (prep != null)
				try {
					prep.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}

	}

	@Override
	public void update(String userId, String fileIdHash, byte[] userFileMetadata) throws RepositoryException {
		// TODO Create a real update query, this is not efficient.

		byte[] retrieveItem = this.select(userId, fileIdHash);

		if (retrieveItem != null) {
			this.delete(userId, fileIdHash);
			this.insert(userId, fileIdHash, userFileMetadata);
		}
	}

	@Override
	public byte[] select(String userId, String fileIdHash) throws RepositoryException {
		byte[] retVal = null;

		String preSqlStmt = String.format("select * from %s where %s = ? and %s = ?", tableName, tableFirstPKName, tableSecondPKName);

		PreparedStatement prep = null;

		try {
			prep = getConnection().prepareStatement(preSqlStmt);
			prep.setString(1, userId);
			prep.setString(2, fileIdHash);

			log.debug("Going to try to retrieve userId [" + userId + "] and fileId [" + fileIdHash + "]");
			
			ResultSet rs = prep.executeQuery();

			if(rs.next()){
				/*
				 * 1. user_id character varying(50) NOT NULL
				 * 2. file_id character varying(50) NOT NULL
				 * 3. file_metadata character varying(2048) NOT NULL
				 */
				
				// Discard this.
				//String userIdRetrieved = rs.getString(1);
				//String fileIdRetrieved = rs.getString(2);
				
				retVal = rs.getString(3).getBytes();
				return retVal;
			}else{
				/* If the result set is empty, then return null */
				//if (!rs.isBeforeFirst()) {
				log.info(String.format("Going to return null, we did not find any object with userId [%s] and fileId [%s]", userId, fileIdHash));
				return null;
			}
			

		} catch (SQLException e) {
			log.error(e.getMessage(), e);
			throw new RepositoryException(String.format("Error when inserting data UUID [%s] for userId [%s]", fileIdHash, userId),
					e);
		} finally {
			if (prep != null)
				try {
					prep.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}

	}

	@Override
	public List<UserFileMetadataWrapper> select(String userId) throws RepositoryException {
		ArrayList<UserFileMetadataWrapper> retVal = new ArrayList<>();

		String preSqlStmt = String.format("select * from %s where %s = ?", tableName, tableFirstPKName);

		PreparedStatement prep = null;

		try {
			prep = getConnection().prepareStatement(preSqlStmt);
			prep.setString(1, userId);

			log.debug("Going to try to retrieve userId [" + userId + "]");
			
			ResultSet rs = prep.executeQuery();

			/* If the result set is empty, then return null */
			if (!rs.isBeforeFirst()) {
				log.info(String.format("Going to return null, we did not find any object with userId [%s]", userId));
				return null;
			}
		
			while(rs.next()){
				/*
				 * 1. user_id character varying(50) NOT NULL
				 * 2. file_id character varying(50) NOT NULL
				 * 3. file_metadata character varying(2048) NOT NULL
				 */
				// Discard this.
				//String userIdRetrieved = rs.getString(1);
				//String fileIdRetrieved = rs.getString(2);
				UserFileMetadataWrapper item = JsonHelper.fromJson(rs.getString(3), UserFileMetadataWrapper.class);
				retVal.add(item);
			}

			return retVal;
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
			throw new RepositoryException(String.format("Error when retrieving data for userId [%s]", userId),
					e);
		} finally {
			if (prep != null)
				try {
					prep.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}
}
